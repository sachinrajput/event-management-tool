package designConf.builder;

public abstract class Activity {
			
	abstract public void get_price();
	abstract public void get_carbonfootprint();
	abstract public void get_time();
	
	abstract public void add_object(String key, Info value);
	abstract public void remove_object(String key);		
	
}
