package designConf.builder;

import designConf.util.Factors;

public class Info implements Factors{	

	
	/*
 	@author ABHISHEK

 	
	A.) Hey I think we will have to forget about factors interface.
	
	This is because, when we had declared the 3 variables(cost,time and cfprint) in 
	Info class, I could not access is it using program to interface principle as the variables 
	were absent in the LHS interface.
	
	On the other hand if we keep these variables in Factors interface, as variables become
	final and need to be accessed in a static way, which is NOT POSSIBLE.
	
	Similarly, if the variables are kept in the Info class, 
	the interface dosen't have much significance i.e. those methods t=dont need a class.			

	B.) Also we have to come up with a way for different argument passing in Info() Constructor.
	Following are the approaches I thought of:
	1. We can send nulls for the variables that are not applicable for some 
		classes - Unnecessarily many NULLS may get added in constructor - when the 
		argument list grows.	
	2. Using your PCT format - but that will increase the length of the "PCT..XYZ" 
		list when variables come into picture.
	3. Using multiple constructors - invalid as new variable(say GYM)may come in that 
		also requires 2 variables like Lab. - NOT APPLICABLE	
	
	* I added enum "units" it takes care of the measuring standards.
	* I'm now using 'ConcurrentHashMap' instead of 'ArrayList'.
	
	We need to address problems A.) and B.), rest I think we are on track.
	Lastly, once we are done with this, we 'll need to test and format/comment all LOC.
	

	*/	
	
	public String datetime = null;
	public String cost = null;
	public String carbonPF = null;

	public Info() {
	
	}


	public Info(String st1) {
		
	}


	public Info(String st1, String st2, String st3) {
	
	}


	public Info(String st1, String st2) {
	
	}

	
	
	@Override
	public String getDatetime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDatetime(String datetimeIn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPrice(String priceIn) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getCarbonfootprint() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCarbonfootprint(String carbonPFIn) {
		// TODO Auto-generated method stub
		
	}	
}
