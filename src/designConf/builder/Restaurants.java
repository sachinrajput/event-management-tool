package designConf.builder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import designConf.util.Factors;
import designConf.util.Results;

public class Restaurants extends Activity {

	private String userKey;
	private Factors rInfo = new Info();
	private Results res =  new Results();

//	private ArrayList<Info> list = new ArrayList<Info>(); 
	private ConcurrentHashMap<String, Info> Map = new ConcurrentHashMap<String, Info>();
	
	public Restaurants(String userKeyIn	) {

		add_object("PS", new Info(50, 1,5));
		add_object("Lemongrass", new Info(22, 35, 2));
		add_object("No. 5", new Info(40, 3, 3));
	
		userKey = userKeyIn;
		
	}

	@Override
	public void get_price() {

	}

	@Override
	public void get_carbonfootprint() {
		
	}

	@Override
	public void get_time() {
		
	}

	@Override
	public void add_object(String key, Info value) {

		Map.put(key, value);

	}
	@Override
	public void remove_object(String key) {
		Map.remove(key);
	}

}
