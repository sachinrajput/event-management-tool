package designConf.builder;

public interface EventActivityInterface {
	public void performActivity(Activity activityObj);
}
