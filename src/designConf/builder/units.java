package designConf.builder;

public enum units {

	standard1("USD", "MINS", "co2"),
	standard2("EURO", "MINS", "co2"),
	standard3("USD", "HRS", "co2"),
	standard4("EURO", "HRS", "co2");  // semi colon here

    private final String cost;
    private final String time;
    private final String cFprint;
//---------    private static final double NO_OF_EXAMS = 3;
    
  // Constructor
    private units(String costIn, String timeIn, String cFprintIn) {
	cost = costIn;
	time = timeIn;
	cFprint = cFprintIn;
	
    }

	public String getcostFormat(){ 
		return cost; 
	}
	
	public String gettimeFormat(){
		return time;
	}
	
	public String getcFprintFormat(){
		return cFprint;
	}


    
}
