package designConf.builder;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import designConf.util.Factors;
import designConf.util.Results;

public class Lab extends Activity{

	private String userKey;
	private Factors lInfo = new Info();
	private Results res =  new Results();

//	private ArrayList<Info> list = new ArrayList<Info>(); 
	private ConcurrentHashMap<String, Info> Map = new ConcurrentHashMap<String, Info>();

	public Lab(String userkeyIn) {

//		Rest_Info("Lemon Grass", 35, 2, 4)
//		Lab_Info(1, 4.0, 7)
//		Pres_Info(1, 5.5)

		add_object("1 Bus", new Info(4.0, 7));
		add_object("1 Elevators", new Info( 5.0, 7));
		add_object("1 Walk",new Info(6.0, 7));

		add_object("2 Bus", new Info(4.0, 7));
		add_object("2 Elevators", new Info( 5.0, 7));
		add_object("2 Walk",new Info(6.0, 7));

		add_object("3 Bus", new Info(4.0, 7));
		add_object("3 Elevators", new Info( 5.0, 7));
		add_object("3 Walk",new Info(6.0, 7));

		userKey = userkeyIn;
		
	}
	
	@Override
	public void get_price() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void get_carbonfootprint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void get_time() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add_object(String key, Info value) {

		Map.put(key, value);

	}
	@Override
	public void remove_object(String key) {
		Map.remove(key);
	}
		
}

