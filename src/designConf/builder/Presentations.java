package designConf.builder;


import designConf.util.Factors;
import designConf.util.Results;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Presentations extends Activity {

	private String userKey;
	private Factors pInfo = new Info();
	private Results res =  new Results();
	
//	private ArrayList<Info> list = new ArrayList<Info>(); 
	private ConcurrentHashMap<String, Info> Map = new ConcurrentHashMap<String, Info>();
	
	public Presentations(String userKeyIn) {

		add_object("1", new Info(5.5));
		add_object("2", new Info(4.4));
		add_object("3", new Info(3.3));
	
		userKey = userKeyIn;
		
	}
	
	@Override
	public void get_price() {

		for (@SuppressWarnings("rawtypes") Map.Entry e : Map.entrySet()) {

			if(userKey.equals(e.getKey())){
				pInfo.cost = ((Info)e.getValue()).cost;
			}
			
		}
		
		
	}

	@Override
	public void get_carbonfootprint() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void get_time() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add_object(String key, Info value) {

		Map.put(key, value);

	}
	@Override
	public void remove_object(String key) {
		Map.remove(key);
	}

}
