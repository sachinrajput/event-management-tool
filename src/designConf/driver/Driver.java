package designConf.driver;


import designConf.builder.EventActivities;
import designConf.builder.EventActivityInterface;
import designConf.builder.Lab;
import designConf.builder.Presentations;
import designConf.builder.Restaurants;
import designConf.factory.Event;
import designConf.factory.EventConferenceFactory;
import designConf.factory.EventFactory;
import designConf.factory.EventList;

public class Driver {

	/**
	 * 
	 *  @param args
	 */
	
	public static void main(String[] args) {		
		/**
		 * Event creation - Factory Pattern Used
		 */

		EventFactory confFactory = new EventConferenceFactory();
		EventList confEvent = new EventList(confFactory);		

		/**
		 * We will take input from user whether his an alumni or not
		 */
		Event conferenceEvent = confEvent.createEvent("Design Pattern Conference", "yes");
		
		/**
		 * We will use builder pattern to implement the activities list
		 */
		
		/**
		 * We can use a different class and create a class of it 
		 * and define all factors / values / arraylist and pass the 
		 * object to the below instantiation of each activity list
		 * for example : 
		 * Factors presentationFactors = new Factors("list of things we need for ppt");
		 * Factors labsessionFactors = new Factors("list of things we need for lab");
		 * Factors restautantsFactors = new Factors("list of things we need for restaurants");
		 * 
		 * conferenceEvent.activityList[0] = new Presentations(presentationFactors);
		 * conferenceEvent.activityList[1] = new LabSessions(labsessionFactors);
		 * conferenceEvent.activityList[2] = new Restaurants(restautantsFactors);
		 */
		
		conferenceEvent.activityList[0] = new Presentations("1 Bus");
		conferenceEvent.activityList[1] = new Lab("2");
		conferenceEvent.activityList[2] = new Restaurants("Lemon Grass");
		
		/**
		 * in for loop perform fixed calculations for all activity - MAIN PURPOSE OF BUILDER!
		 */
		
		EventActivityInterface act = new EventActivities();
		
		for (int i = 0; i < conferenceEvent.activityList.length; i++) {
			act.performActivity(conferenceEvent.activityList[i]);
		}
		
		/**
		 * After above steps we will have the data in Results class or may be somewhere 
		 * So, lets print that .....
		 * call some display functions ... now we can handle the different ways of handling factors
		 * in display()
		 */
		
	}

}
