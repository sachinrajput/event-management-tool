package designConf.factory;

import designConf.builder.Activity;

public abstract class Event {
	
	public String eventName;
	
	public Activity activityList[];
	
	public abstract void register();
}
