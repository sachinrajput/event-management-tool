package designConf.factory;

public class EventFactory {
	/**
	 * EventFactory confFactory = new EventConferenceFactory();
	 * Event event = new Event(confFactory);//"Design Pattern Conference"
	 * event.register("Design Pattern Conference");  
	 */
	
	private Event event;

	public Event createEvent(String eventNameIn, String alumniStudent) {
		// TODO Auto-generated method stub
		
		if(alumniStudent.equals("yes"))
			event = new AlumniStudent();
		else if(alumniStudent.equals("no"))
			event = new NormalStudent();
		
		event.eventName = eventNameIn;
		
		return event;
	}

	
}
