package designConf.factory;

public class EventList {
	private EventFactory factory;
	
	public EventList(EventFactory factoryIn){
		this.factory = factoryIn;
	}
	
	public Event createEvent(String eventNameIn, String alumniStudent){
		Event event = factory.createEvent(eventNameIn,alumniStudent);
		return event;
	}
}
