package designConf.util;

import java.sql.Date;

import designConf.builder.units;

/**
 * This will have all factors affecting the activities
 * their default printing ways 
 * and overridden ways as well
 * get and set for factors
 *
 * @author sachin
 *
 */
public interface Factors {

	
//	public String datetime = null;
//	public String cost = null;
//	public String carbonPF = null;
	
	static String currencyFormat = units.standard1.getcostFormat();
	static String timeFormat = units.standard1.gettimeFormat();
	static String carbonPFFormat = units.standard1.getcFprintFormat();
	
	/**
	 * @return the datetime
	 */
	public String getDatetime() ;
/*	{
		/**
		 * Add more formats and just set the static variable 
		 * to required output
		 * If required add functions to calculate it
		 */
/*		if(timeFormat.equals("mins")){
			datetime = datetime + ((Integer.parseInt(datetime) > 1 )?" mins":"min"); 
		} else if(timeFormat.equals("hrs")){
			return getHrs(datetime);
		}
		return datetime;
	}*/
	/**
	 * @param datetime the datetime to set
	 */
	public void setDatetime(String datetimeIn);
	/*{
		this.datetime = datetimeIn;
	}
	/**
	 * @return the price
	 */

	public String getPrice();
	/*{
		/**
		 * Add more conditions here to handle the new formats 
		 */
/*		if(currencyFormat == "USD")
			price = "$" + price;
		else if(currencyFormat == "EURO")
			price = price + " EURO";
		return price;
	}
	/**
	 * @param price the price to set
	 */
	
	
	public void setPrice(String priceIn);
	/*{
		this.price = priceIn;
	}
	/**
	 * @return the carbonfootprint
	 */

	public String getCarbonfootprint();
	/*{
		/**
		 * we can add more conditions based on which we can change the output
		 */
		/*if(carbonPFFormat == "CO2")
			carbonPF = carbonPF + " in CO2";
		return carbonPF;
	}
	/**
	 * @param carbonfootprint the carbonfootprint to set
	 */
	
	
	public void setCarbonfootprint(String carbonPFIn);
	/*{
		this.carbonPF = carbonPFIn;
	}
	
	
	public String getHrs(String time){
		int hrs;
		int mins;
		hrs = (Integer.parseInt(time) / 60);
		mins = (Integer.parseInt(time) % 60);
		return (String.valueOf(hrs)+":"+String.valueOf(mins));
	}
	*/
	
}

